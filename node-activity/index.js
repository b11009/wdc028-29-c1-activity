fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => { 
	console.log(`title:\n${json.title} \nbody:\n${json.body}`)
});

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Change Title',
		body:'Change Body'
	}),
})
.then((response)=> response.json())
.then((json)=> console.log(json));