// Use the "require" directive to load the express module/package
const express = require ("express");

// Create an application using express
const app = express();

// Port to listen to
const port = 4001;

// Setup or allowing the server to handle data from requests
// allows your app to read json data
app.use(express.json());

// Allows the app to read data from forms
// Applying the option "extended:true()" allows us to receive information in other data types such as an object
app.use(express.urlencoded({extended:true}));

let users = [];

app.post("/register", (req, res) => {
	console.log(req.body);

	if(req.body.firstname !== '' && req.body.lastname !== '' 
		&& req.body.username !== '' && req.body.password !== ''){

		users.push(req.body);
		res.send(`Thank you for signing up, ${req.body.firstname}!`);
	} else {
		res.send("Please input complete information.");
	}
});

app.get("/users", (req, res) => {
	res.json(users);
});

app.post("/login", (req, res) =>{
	// Create variable to store the message to be sent back to the client
	let message;

	// Create for loop that will loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++){

		if(users.find(
			user => user.username === req.body.username && 
			user.password === req.body.password)) {

			message = `Hi ${req.body.username}, you are successfully login.`
		}
		else if(req.body.username == ''|| 
			req.body.password == '') {

			message = "Please input Username or Password."


		} else {
			message = "Incorrect Password or Username";
		}
	}
	res.send(message);
});

// Tells our server to listened to the port
app.listen(port,()=>console.log(`Server running at port ${port}`));

